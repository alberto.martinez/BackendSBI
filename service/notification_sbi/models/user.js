const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    username: String,
    permissions: String,
    role: String,
    hash: String,
    salt: String,
  },
  { strict: false }
);

mongoose.model("User", UserSchema);
