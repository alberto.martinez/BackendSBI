const express = require("express");
const router = express.Router();
const Controllers = require("../Controllers/controllers.notification");

// const CheckToken = require("../../config/checkToken");

const catchErrAsync = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch((err) => next(err));
  };
};

const catchErr = (fn) => {
  return (req, res, next) => {
    try {
      fn(req, res, next);
    } catch (err) {
      next(err);
      //console.log(err)
    }
  };
};

router
  .route("/device")
  .get(catchErrAsync(Controllers.GetDevice))
  .post(catchErrAsync(Controllers.AddDevice))
  .delete(catchErrAsync(Controllers.DeleteDevice));

router
  .route("")
  .get(catchErrAsync(Controllers.GetNotification))
  .post(catchErrAsync(Controllers.SendNotification));

// router
//   .route("/user")
//   .get(catchErrAsync(Controllers.GetDevice))
//   .post(catchErrAsync(Controllers.AddDevice))
//   .delete(catchErrAsync(Controllers.DeleteDevice));

module.exports = {
  router: router,
};
