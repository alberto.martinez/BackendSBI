const utils = require("../../helpers/helpers");
const router = require("express").Router();
var MongoWraper = require("../../helpers/mongoFunctions");
const { ObjectID } = require("mongodb");

const { ApiError } = require("../ApiError");

const collectionNotificationDevices = "NotificationDevices";
const Firebase = require("../../helpers/Firebase");
// Example Register Enterprise
// {
//   "EnterpriseName": "Absa",
//   "Location": [
//       {
//           "LocationName":"Lobby 33",
//           "Latitude": "1273719",
//           "Longitude": "1927365"
//       }
//   ]
// }

// Example Register User
// {
//   "UserName":"alberto.maor21@gmail.com",
//   "Email":"alberto.maor21@gmail.com",
//   "Password":"@123Qwer",
//   "Name":"Alberto Martinez Ortega",
//   "Role":"Software Lord and Friend"
// }

const SendNotification = async (req, res) => {
  // Here you could receive the token id, titulo and mensage, in this case all these params are already hardcoded
  const Token =
    "dormiJuQRKS8tOGmBx8XtV:APA91bGU4AmCEntNDmNPADXfh3hoEe17s5PX2sgGiHHalSuUBR0ccaAC1qRvPE69Aa3N_3zBFGbIDg4Y1j2-B5w7-06deCXA_6FyIGJBGxIQqP3A327naJr2q7MZEuA-WBYmH3K8oeBR";

  const data = {
    titulo: "Re:Hola papu",
    mensaje: "Ajua",
  };
  const notification = {
    notification: {
      title: "??Papu",
      body: "esta ahi??",
    },
    data: data,
  };

  Firebase.sendPushToOneUser(Token, notification);
  res.send("Sending Notification to One user...");
};
const GetNotification = () => {};

const AddDevice = async (req, res) => {
  console.log("adding device");
  const RequestUser = JSON.parse(req.headers.user);
  const RequestEnterprise = JSON.parse(req.headers.enterprise);
  const DeviceExist = await MongoWraper.FindOne(
    {
      $and: [{ UserEmail: RequestUser.Email }, { DeviceID: req.body.DeviceID }],
    },
    collectionNotificationDevices,
    RequestEnterprise.Db
  );
  console.log(DeviceExist);
  if (DeviceExist != null) {
    const Notification = await MongoWraper.UpdateMongo(
      {
        $and: [
          { UserEmail: RequestUser.Email },
          { DeviceID: req.body.DeviceID },
        ],
      },
      {
        Token: req.body.Token,
      },
      collectionNotificationDevices,
      RequestEnterprise.Db
    );
  } else {
    const Notification = await MongoWraper.SavetoMongo(
      {
        DeviceID: req.body.DeviceID,
        Token: req.body.Token,
        UserId: RequestUser._id,
        UserEmail: RequestUser.Email,
      },
      collectionNotificationDevices,
      RequestEnterprise.Db
    );
  }
  res.json({
    status: "ok",
    message: "Divice added",
  });
};
const DeleteDevice = async (req, res) => {
  console.log("deleating device");
  const RequestUser = JSON.parse(req.headers.user);
  const RequestEnterprise = JSON.parse(req.headers.enterprise);
  const Devices = await MongoWraper.DeleteMongo(
    {
      $and: [{ UserEmail: RequestUser.Email }, { DeviceID: req.body.DeviceID }],
    },
    collectionNotificationDevices,
    RequestEnterprise.Db
  );
  res.json({
    status: "ok",
    message: "Divice deleated",
  });
};

const GetDevice = async (req, res) => {
  const RequestUser = JSON.parse(req.headers.user);
  const RequestEnterprise = JSON.parse(req.headers.enterprise);
  const Devices = await MongoWraper.FindManyLimit(
    { UserEmail: RequestUser.Email },
    1000,
    collectionNotificationDevices,
    RequestEnterprise.Db
  );

  res.json({
    status: "ok",
    message: "Divice added",
    data: Devices,
  });
};

exports.GetNotification = GetNotification;
exports.SendNotification = SendNotification;
exports.AddDevice = AddDevice;
exports.DeleteDevice = DeleteDevice;
exports.GetDevice = GetDevice;
