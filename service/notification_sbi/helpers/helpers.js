const crypto = require("crypto");
const jsonwebtoken = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");

//const pathToKey = path.join(__dirname, '..', 'id_rsa_priv.pem');
//const PRIV_KEY = fs.readFileSync(pathToKey, 'utf8');

/**
 * -------------- HELPER FUNCTIONS ----------------
 */

/**
 *
 * @param {*} password - The plain text password
 * @param {*} hash - The hash stored in the database
 * @param {*} salt - The salt stored in the database
 *
 * This function uses the crypto library to decrypt the hash using the salt and then compares
 * the decrypted hash/salt with the password that the user provided at login
 */
function validPassword(password, hash, salt) {
  var hashVerify = crypto
    .pbkdf2Sync(password, salt, 10000, 64, "sha512")
    .toString("hex");
  return hash === hashVerify;
}

/**
 *
 * @param {*} password - The password string that the user inputs to the password field in the register form
 *
 * This function takes a plain text password and creates a salt and hash out of it.  Instead of storing the plaintext
 * password in the database, the salt and hash are stored for security
 *
 * ALTERNATIVE: It would also be acceptable to just use a hashing algorithm to make a hash of the plain text password.
 * You would then store the hashed password in the database and then re-hash it to verify later (similar to what we do here)
 */
function genPassword(password) {
  var salt = crypto.randomBytes(32).toString("hex");
  var genHash = crypto
    .pbkdf2Sync(password, salt, 10000, 64, "sha512")
    .toString("hex");

  return {
    salt: salt,
    hash: genHash,
  };
}

function testAES(phrase) {
  const ENC_KEY = "bf3c199c2470cb477d907b1e0917c17b"; // set random encryption key
  const IV = "5183666c72eec9e4"; // set random initialisation vector
  // ENC_KEY and IV can be generated as crypto.randomBytes(32).toString('hex');

  //const phrase = "s@GRX-RTY-10002";

  var encrypt = (val) => {
    let cipher = crypto.createCipheriv("aes-256-cbc", ENC_KEY, IV);
    let encrypted = cipher.update(val, "utf8", "base64");
    encrypted += cipher.final("base64");
    return encrypted;
  };

  var decrypt = (encrypted) => {
    let decipher = crypto.createDecipheriv("aes-256-cbc", ENC_KEY, IV);
    let decrypted = decipher.update(encrypted, "base64", "utf8");
    return decrypted + decipher.final("utf8");
  };

  encrypted_key = encrypt(phrase);
  console.log(encrypted_key);
  original_phrase = decrypt(encrypted_key);
  console.log(original_phrase);
}

function decryptAES(crypted) {
  const ENC_KEY = "bf3c199c2470cb477d907b1e0917c17b"; // set random encryption key
  const IV = "5183666c72eec9e4"; // set random initialisation vector
  // ENC_KEY and IV can be generated as crypto.randomBytes(32).toString('hex');

  //const phrase = "s@GRX-RTY-10002";

  var decrypt = (encrypted) => {
    let decipher = crypto.createDecipheriv("aes-256-cbc", ENC_KEY, IV);
    let decrypted = decipher.update(encrypted, "base64", "utf8");
    return decrypted + decipher.final("utf8");
  };

  return decrypt(crypted);
  //console.log(original_phrase);
}

/**
 * @param {*} user - The user object.  We need this to set the JWT `sub` payload property to the MongoDB user ID
 */
function issueJWT(user) {
  const _id = user._id;

  const expiresIn = "1h";

  const payload = {
    sub: _id,
  };

  const signedToken = jsonwebtoken.sign(payload, "miclaveultrasecreta123", {
    expiresIn: expiresIn,
  });

  return {
    token: "Bearer " + signedToken,
    expires: expiresIn,
  };
}

module.exports.validPassword = validPassword;
module.exports.genPassword = genPassword;
module.exports.issueJWT = issueJWT;
module.exports.testAES = testAES;
module.exports.decryptAES = decryptAES;
