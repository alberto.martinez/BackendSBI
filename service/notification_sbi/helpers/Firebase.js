var admin = require("firebase-admin");

const serviceAccount = require("./knesys-plus-techcon-firebase-adminsdk-ga2hx-85aa6671d8.json");

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
// });

function initFirebase() {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}

initFirebase();

const notification_options = {
  priority: "high",
  // timeToLive: 60 * 60 * 24,
  timeToLive: 40,
};

function sendPushToOneUser(token, notification) {
  admin
    .messaging()
    .sendToDevice(token, notification, notification_options)
    .then((response) => {
      // Response is a message ID string.
      console.log("Successfully sent message:", response);
    })
    .catch((error) => {
      console.log("Error sending message:", error);
    });
}

function sendPushToTopic(notification) {
  const message = {
    topic: notification.topic,
    data: {
      titulo: notification.titulo,
      mensaje: notification.mensaje,
    },
  };
  sendMessage(message);
}

module.exports = { sendPushToOneUser, sendPushToTopic };

function sendMessage(message) {
  admin
    .messaging()
    .send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log("Successfully sent message:", response);
    })
    .catch((error) => {
      console.log("Error sending message:", error);
    });
}
