// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");

module.exports = (req, res, next) => {
  // const token = req.headers['access-token'];
  var token = req.headers.authorization;
  token = token.replace("Bearer ", "");
  console.log(token);
  if (token) {
    jwt.verify(token, "miclaveultrasecreta123", (err, decoded) => {
      console.log(decoded);
      if (err) {
        return res.json({ mensaje: "Token inválida" });
      } else {
        MongoWraper.FindIDOne(decoded.sub, "Users").then((user) => {
          console.log(user);
          console.log(decoded);
          //req.decoded = decoded;
          req.user = user;
          next();
        });
      }
    });
  } else {
    res.send({
      mensaje: "Token no proveída.", // agregar status http
    });
  }
};
