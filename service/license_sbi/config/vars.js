require("dotenv").config();

module.exports = {
  env: process.env.NODE_ENV,
  mongo: {
    uri:
      process.env.NODE_ENV === "test"
        ? process.env.MONGO_URI
        : process.env.MONGO_URI,
  },
  mongoDb: process.env.MONGO_DB_MANAGEMENT,
  kServer: {
    uri:
      process.env.NODE_ENV === "development"
        ? process.env.KNESYS_SERVER_DEV
        : process.env.KNESYS_SERVER_PROD,
  },
  logs: process.env.NODE_ENV === "production" ? "combined" : "dev",
  port: process.env.PORT,
};
