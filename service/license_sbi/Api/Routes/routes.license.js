const express = require("express");
const router = express.Router();
const Controllers = require("../Controllers/controllers.license");
// const CheckToken = require("../../config/checkToken");

const catchErrAsync = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch((err) => next(err));
  };
};

const catchErr = (fn) => {
  return (req, res, next) => {
    try {
      fn(req, res, next);
    } catch (err) {
      next(err);
      //console.log(err)
    }
  };
};

router
  .route("/")
  .post(catchErrAsync(Controllers.Register))
  .get(catchErrAsync(Controllers.GetLicense));

router.route("/validate").post(catchErrAsync(Controllers.ValidateLicense));
router.route("/exist").post(catchErrAsync(Controllers.ValidateLicenseExist));
router
  .route("/validate/module")
  .post(catchErrAsync(Controllers.ValidateModule));

module.exports = {
  router: router,
};
