const utils = require("../../helpers/helpers");
const router = require("express").Router();
var MongoWraper = require("../../helpers/mongoFunctions");
const { ObjectID } = require("mongodb");

const { ApiError } = require("../ApiError");
const collectionLicense = "License";
const { GetRandom6 } = require("../helpersApi/RegisterUser");

// Example Register License
// {
//   "EnterpriseName": "axie",
//   "StartDate": "10/11/2003",
//   "EndDate": "8/5/2021",
//   "Modules": {
//       "Notification": {
//           "StartDate": "10/11/2003",
//           "EndDate": "8/5/2021"
//       }
//   },
//   "Extra": "nice company"
// }

const Register = async (req, res, next) => {
  console.log("Registrando licencia");

  const RequestUser = JSON.parse(req.headers.user);
  if (RequestUser.AppProfile != "adminVentas") {
    throw new ApiError(400, "User is not adminVentas");
  }
  const License = await MongoWraper.SavetoMongo(
    {
      EnterpriseName: req.body.EnterpriseName,
      StartDate: req.body.StartDate,
      EndDate: req.body.EndDate,
      Modules: req.body.Modules,
      Extra: req.body.Extra,
    },
    collectionLicense
  );
  console.log(License.ops);
  const LicenseAdded = License.ops[0];

  res.json({
    status: "ok",
    message: "License Added",
    data: {
      StartDate: LicenseAdded.StartDate,
      EndDate: LicenseAdded.EndDate,
      License: LicenseAdded._id,
    },
  });
};

const GetLicense = async (req, res) => {
  const RequestUser = JSON.parse(req.headers.user);
  if (RequestUser.AppProfile != "adminVentas") {
    throw new ApiError(400, "User is not adminVentas");
  }
  const andArr = Object.keys(req.query).map((prop, index) =>
    req.query[prop] == { [prop]: { $regex: "", $options: "si" } }
      ? []
      : req.query[prop].split(" ").map((word) => {
          return { [prop]: { $regex: word, $options: "si" } };
        })
  );
  const fullAndArray = andArr.reduce((acum, arr) => [...acum, ...arr], []);
  const exampleQuerie = {
    $and: [...fullAndArray],
  };
  console.log(exampleQuerie);
  const License = await MongoWraper.GetAll(collectionLicense);
  const finalArr = await MongoWraper.FindManyLimit(
    exampleQuerie,
    1000,
    collectionLicense
  );
  console.log(finalArr);

  console.log(License);
  res.json({
    status: "ok",
    data: finalArr,
  });
};

const ValidateLicenseExist = async (req, res) => {
  console.log("entre al license exist");
  console.log(req.body.LicenseId);
  try {
    const License = await MongoWraper.FindIDOne(
      req.body.LicenseId.toString(),
      collectionLicense
    );
    console.log(License);
    //const LicenseFound = License[0];
    if (License.length == 0) {
      res.json({
        status: "ok",
        data: { Exist: false },
      });
    }
    res.json({
      status: "ok",
      data: { Exist: true },
    });
  } catch (err) {
    res.json({
      status: "ok",
      data: { Exist: false },
    });
  }
};

const ValidateLicense = async (req, res) => {
  console.log(req.headers.user);
  const RequestUser = JSON.parse(req.headers.user);
  if (!RequestUser.EnterpriseId) {
    throw new ApiError(500, "User needs an enterprise to use this feature");
  }
  const License = await MongoWraper.FindIDOne(
    RequestUser.LicenseId,
    collectionLicense
  );
  console.log(License);
  const LicenseFound = License[0];
  console.log(new Date());
  console.log(new Date(LicenseFound.EndDate));
  const SecondsRemaining = new Date(LicenseFound.EndDate) - new Date();
  const SecondsLeft = new Date(LicenseFound.StartDate) - new Date();
  console.log(SecondsRemaining);
  console.log(SecondsLeft);
  const DaysRemaining = Math.floor(
    parseInt(SecondsRemaining / 1000) / (3600 * 24) + 1
  );
  const DaysLeft = Math.floor(parseInt(SecondsLeft / 1000) / (3600 * 24));
  console.log(DaysRemaining);
  console.log(DaysLeft);
  const Active = DaysRemaining >= 0 && DaysLeft <= 0 ? true : false;
  res.json({
    status: "ok",
    data: { Active, DaysRemaining, DaysLeft, LicenseData: License },
  });
};
const ValidateModule = async (req, res) => {
  console.log("entre");
  const RequestUser = JSON.parse(req.headers.user);
  if (!RequestUser.EnterpriseId) {
    throw new ApiError(500, "User needs an enterprise to use this feature");
  }
  if (!req.body.Module) {
    throw new ApiError(400, "Module not in request");
  }
  const Module = req.body.Module;

  const License = await MongoWraper.FindIDOne(
    RequestUser.LicenseId,
    collectionLicense
  );
  console.log(License);
  const LicenseFound = License[0];

  if (!LicenseFound.Modules.hasOwnProperty(Module)) {
    throw new ApiError(500, "Module not in license");
  }
  const LicenseModule = LicenseFound.Modules[Module];
  const SecondsRemaining = new Date(LicenseFound.EndDate) - new Date();
  const SecondsLeft = new Date(LicenseFound.StartDate) - new Date();
  console.log(SecondsRemaining);
  console.log(SecondsLeft);
  const DaysRemaining = Math.floor(
    parseInt(SecondsRemaining / 1000) / (3600 * 24) + 1
  );
  const DaysLeft = Math.floor(parseInt(SecondsLeft / 1000) / (3600 * 24));
  console.log(DaysRemaining);
  console.log(DaysLeft);
  const Active = DaysRemaining >= 0 && DaysLeft <= 0 ? true : false;

  res.json({
    status: "ok",
    data: { Active, DaysRemaining, DaysLeft, LicenseData: LicenseModule },
  });
};
exports.ValidateLicenseExist = ValidateLicenseExist;
exports.ValidateModule = ValidateModule;
exports.ValidateLicense = ValidateLicense;
exports.Register = Register;
exports.GetLicense = GetLicense;
