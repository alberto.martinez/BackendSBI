class ApiError extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}
const handleError = (err, res) => {
  const statusCode = err.statusCode || 500;
  const message = err.message || "Unhandled Error";
  console.log(err);
  res.status(statusCode).json({
    status: "error",
    errors: message,
  });
};

exports.ApiError = ApiError;
exports.handleError = handleError;
