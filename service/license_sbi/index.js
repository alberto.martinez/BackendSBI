const util = require("util");
const Mongowraper = require("./helpers/mongoFunctions");
const express = require("express");
const morgan = require("morgan");
var bodyParser = require("body-parser");
const app = express();
const { handleError } = require("./Api/ApiError");
const cors = require("cors");
app.use(morgan("combined"));
//setings
app.set("port", 1010);
//middlewares
//app.use(morgan('dev'));

app.use(
  bodyParser.json({
    limit: "10mb",
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "10mb",
    parameterLimit: 1000000,
    extended: true,
  })
);

// app.use(cors());

//app.use('/api', require('./status.routes'));

//app.use('/api/snmp', require('./Api/routes.operations'));

app.use("/", require("./Api/Routes/routes.license").router);

app.use((err, req, res, next) => {
  handleError(err, res);
});
app.listen(app.get("port"), () => {
  console.log(`server on port ${app.get("port")}`);
});
