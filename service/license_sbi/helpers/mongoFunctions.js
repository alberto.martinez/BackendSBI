// mongofunctions.js
const { mongo, mongoDb } = require("../config/vars");
const { MongoClient, Timestamp } = require("mongodb");
var ObjectId = require("mongodb").ObjectID;

async function FindIDOne(Id, collection, databaseName) {
  const DatabaseName = databaseName == null ? mongoDb : databaseName;
  var o_id = new ObjectId(Id);
  const query = { _id: o_id };
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo
      .collection(collection)
      .find(query)
      .limit(1)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}
function SavetoMongoCallback(objectToSave, collection, databaseName) {
  const DatabaseName = databaseName == null ? mongoDb : databaseName;
  MongoClient.connect(mongo.uri, { useUnifiedTopology: true }, (err, db) => {
    if (err) console.log(err);
    const dbo = db.db(DatabaseName);
    dbo.collection(collection).insertOne(objectToSave, (err2) => {
      if (err2) console.log(err2);
      console.log("inserted");
      //console.log(`- 1 Device inserted: ${finalObject.macdeviceid} -- ${finalObject.sensor.counter.value}`);
      db.close();
    });
  });
}

async function SavetoMongo(objectToSave, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo.collection(collection).insertOne(objectToSave);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}

async function AggregationMongo(arrAggregation, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo
      .collection(collection)
      .aggregate(arrAggregation, { allowDiskUse: true })
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log("errooooooor");
    console.log(error);
    return [];
  }
}

async function DeleteMongo(query, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo.collection(collection).deleteOne(query);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}

async function UpdateMongo(query, newProperties, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    var newvalues = { $set: newProperties };
    let result = await dbo.collection(collection).updateOne(query, newvalues);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}

function DeleteMongoCallback(idObjectToDelete, collection, databaseName) {
  const DatabaseName = databaseName == null ? mongoDb : databaseName;
  MongoClient.connect(mongo.uri, { useUnifiedTopology: true }, (err, db) => {
    if (err) console.log(err);
    const dbo = db.db(DatabaseName);
    var myquery = { _id: idObjectToDelete };
    dbo.collection(collection).deleteOne(myquery, (err2) => {
      if (err2) console.log(err2);
      console.log("deleted");
      //console.log(`- 1 Device inserted: ${finalObject.macdeviceid} -- ${finalObject.sensor.counter.value}`);
      db.close();
    });
  });
}

async function GetLastMongo(limit, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo
      .collection(collection)
      .find()
      .sort({ _id: 1 })
      .limit(limit)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

async function FindOne(query, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo.collection(collection).findOne(query);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return {};
  }
}

async function FindManyLimit(query, limit, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo
      .collection(collection)
      .find(query)
      .limit(limit)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

async function GetAll(collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo
      .collection(collection)
      .find({})
      .sort({ _id: 1 })
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}
async function FindLimitLast(query, limit, collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo
      .collection(collection)
      .find(query)
      .sort({ _id: -1 })
      .limit(limit)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

async function DropCollection(collection, databaseName) {
  try {
    const DatabaseName = databaseName == null ? mongoDb : databaseName;
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(DatabaseName);
    let result = await dbo.collection(collection).drop();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}
exports.AggregationMongo = AggregationMongo;
exports.UpdateMongo = UpdateMongo;
exports.FindOne = FindOne;
exports.FindIDOne = FindIDOne;
exports.GetAll = GetAll;
exports.SavetoMongoCallback = SavetoMongoCallback;
exports.SavetoMongo = SavetoMongo;
exports.DropCollection = DropCollection;
exports.DeleteMongoCallback = DeleteMongoCallback;
exports.DeleteMongo = DeleteMongo;
exports.GetLastMongo = GetLastMongo;
exports.FindManyLimit = FindManyLimit;
exports.FindLimitLast = FindLimitLast;
