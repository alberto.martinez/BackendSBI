const utils = require("../../helpers/helpers");
const router = require("express").Router();
var MongoWraper = require("../../helpers/mongoFunctions");
const { ObjectID } = require("mongodb");
const { ApiError } = require("../ApiError");
var QRCode = require("qrcode");
const {
  RegisterUser,
  InvitationMail,
  GetRandom6,
  ReSendValidation,
} = require("../helpersApi/RegisterUser");
const axios = require("axios");
const collectionUsers = "Users";
const collectionInvitation = "Invitation";

function ValidateLogin(req) {
  console.log(req.body);
  if (!req.body.hasOwnProperty("Email")) {
    throw new ApiError(400, "Email required!");
  }
  if (!req.body.hasOwnProperty("Password")) {
    throw new ApiError(400, "Password required!");
  }
}

const Login = async (req, res) => {
  console.log("login");
  console.log(req.body);
  ValidateLogin(req);
  //mofificacion usuario por username para no moverle a adira

  const user = await MongoWraper.FindOne(
    { Email: req.body.Email },
    collectionUsers
  );
  console.log(user);
  if (!user) {
    console.log("Error de usuario no encontrado");
    throw new ApiError(401, "User not found");
  }
  if (!user.Active) {
    throw new ApiError(401, "User not active");
  }

  // Function defined at bottom of app.js
  const isValid = utils.validPassword(req.body.Password, user.hash, user.salt);
  if (user.EnterpriseId) {
    const URILicense = "http://license_tech:1010/validate/";
    const VerifyResponse = await axios.post(
      URILicense,
      {},
      {
        headers: {
          user: JSON.stringify(user),
        },
      }
    );
    console.log(VerifyResponse.data.data);
    if (!VerifyResponse.data.data.Active) {
      throw new ApiError(402, "License not active");
    }
  }

  if (isValid) {
    const tokenObject = utils.issueJWT(user);

    res.json({
      status: "ok",
      message: "Token generated",
      data: { token: tokenObject.token, expiresIn: tokenObject.expires, user },
    });
  } else {
    throw new ApiError(400, "You entered the wrong password");
  }
};
function ValidateRegister(req) {
  if (!req.body.hasOwnProperty("Email")) {
    throw new ApiError(400, "Email required!");
  }
  if (!req.body.hasOwnProperty("Password")) {
    throw new ApiError(400, "Password required!");
  }
  if (!req.body.hasOwnProperty("Name")) {
    throw new ApiError(400, "Name required!");
  }
}

const Register = async (req, res, next) => {
  ValidateRegister(req);
  await RegisterUser({
    Email: req.body.Email,
    Password: req.body.Password,
    Name: req.body.Name,
  });
  res.json({
    status: "ok",
    message: "User added",
  });
};
const RegisterKnesys = async (req, res, next) => {
  ValidateRegister(req);
  await RegisterUser({
    Email: req.body.Email,
    Password: req.body.Password,
    Name: req.body.Name,
    AppProfile: req.body.AppProfile,
  });
  res.json({
    status: "ok",
    message: "User added",
  });
};

// {
//   "Email": "alberto.maor21@gmail.com",
//   "AppProfile": "recidente",
// },
function ValidateInviteUser(req) {
  if (!req.body.hasOwnProperty("Email")) {
    throw new ApiError(400, "Email required!");
  }
  if (!req.body.hasOwnProperty("AppProfile")) {
    throw new ApiError(400, "AppProfile required!");
  }
}
const InviteUser = async (req, res, next) => {
  ValidateInviteUser(req);
  console.log(req.headers);
  const Enterprise = JSON.parse(req.headers.enterprise);
  console.log(Enterprise);
  const Code = GetRandom6().toString();
  const InvitationCode = await MongoWraper.SavetoMongo(
    {
      Email: req.body.Email,
      AppProfile: req.body.AppProfile,
      EnterpriseId: Enterprise._id,
      EnterpriseName: Enterprise.EnterpriseName,
      LicenseId: Enterprise.LicenseId,
      Code: Code,
    },
    collectionInvitation
  );

  InvitationMail(
    req.body.Email,
    Enterprise.EnterpriseName,
    Code,
    req.body.AppProfile
  );
  res.json({
    status: "ok",
    message: "Invitation sent",
    data: {
      Email: req.body.Email,
      AppProfile: req.body.AppProfile,
      EnterpriseId: Enterprise._Id,
      EnterpriseName: Enterprise.EnterpriseName,
      LicenseId: Enterprise.LicenseId,
      Code: Code,
    },
  });
};
// {
//   "Email": req.body.Email
//   "Code": req.body.Code
// }

function ValidateRegisterInvitation(req) {
  if (req.body.Code) {
    throw new ApiError(400, "Code required!");
  }
}
const RegisterInvitation = async (req, res, next) => {
  const User = JSON.parse(req.headers.user);
  console.log(User.Email);
  console.log(req.body.Code);
  const Invitation = await MongoWraper.FindOne(
    { $and: [{ Email: User.Email }, { Code: req.body.Code }] },
    // { Email: User.Email },
    // { Code: req.body.Code },
    collectionInvitation
  );
  console.log(Invitation);
  if (Invitation == null) {
    throw new ApiError(400, "Code not valid");
  }

  const UserEnterprise = await MongoWraper.UpdateMongo(
    { Email: User.Email },
    {
      AppProfile: Invitation.AppProfile,
      EnterpriseId: Invitation.EnterpriseId,
      EnterpriseName: Invitation.EnterpriseName,
      LicenseId: Invitation.LicenseId,
    },
    collectionUsers
  );
  res.json({
    status: "ok",
    message: "User joined " + Invitation.EnterpriseName,
  });
};

const Profile = (req, res) => {
  //something to do.
  // printing the user data

  console.log(JSON.parse(req.headers.user));

  // sending the user data
  res.json({
    status: "ok",
    message: "User found",
    data: JSON.parse(req.headers.user),
  });
};
// Example
// {
//   "ActivationCode":"",
//   "Email":""
// }
const ActivateUser = async (req, res) => {
  const User = await MongoWraper.FindOne(
    { Email: req.body.Email },
    collectionUsers
  );
  console.log(User);
  if (User == null) {
    throw new ApiError(401, "User not found");
  }
  if (User.Active) {
    throw new ApiError(500, "User is already active");
  }
  if (User.LastValidationCode != req.body.ActivationCode) {
    throw new ApiError(500, "Activation code not valid");
  }
  await MongoWraper.UpdateMongo(
    { Email: req.body.Email },
    { Active: true },
    collectionUsers
  );
  // sending the user data
  res.json({
    status: "ok",
    message: "Account validated",
  });
};

const ReSendCode = async (req, res, next) => {
  await ReSendValidation(req.body.Email);

  res.json({
    status: "ok",
    message: "Validation code sent",
  });
};

exports.RegisterKnesys = RegisterKnesys;
exports.ReSendCode = ReSendCode;
exports.ActivateUser = ActivateUser;
exports.RegisterInvitation = RegisterInvitation;
exports.InviteUser = InviteUser;
exports.Login = Login;
exports.Profile = Profile;
exports.Register = Register;
