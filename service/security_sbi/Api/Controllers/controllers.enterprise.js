const utils = require("../../helpers/helpers");
const router = require("express").Router();
var MongoWraper = require("../../helpers/mongoFunctions");
const { ObjectID } = require("mongodb");
const axios = require("axios");
const { ApiError } = require("../ApiError");
const collectionEnterprise = "Enterprise";
const collectionUsers = "Users";
const { GetRandom6 } = require("../helpersApi/RegisterUser");

// Example Register Enterprise
// {
//   "EnterpriseName": "Absa",
//   "LicenseId":"61084bdf2c785a0033b1493b",
//   "Location": [
//       {
//           "LocationName":"Lobby 33",
//           "Latitude": "1273719",
//           "Longitude": "1927365"
//       }
//   ]
// }

// Example Register User
// {
//   "UserName":"alberto.maor21@gmail.com",
//   "Email":"alberto.maor21@gmail.com",
//   "Password":"@123Qwer",
//   "Name":"Alberto Martinez Ortega",
//   "Role":"Software Lord and Friend"
// }

const Register = async (req, res, next) => {
  console.log("Registrando empresa");

  const RequestUser = JSON.parse(req.headers.user);
  if (RequestUser.EnterpriseId != null) {
    throw new ApiError(400, "User already has enterprise");
  }
  const URILicense = "http://license_tech:1010/exist";
  const VerifyResponse = await axios.post(URILicense, {
    LicenseId: req.body.LicenseId,
  });
  console.log(VerifyResponse.data);
  if (!VerifyResponse.data.data.Exist) {
    throw new ApiError(402, "License not exist");
  }
  const RandomNumber = GetRandom6();
  const Enterprise = await MongoWraper.SavetoMongo(
    {
      EnterpriseName: req.body.EnterpriseName,
      LicenseId: req.body.LicenseId,
      Location: req.body.Location,
      Db: req.body.EnterpriseName + RandomNumber,
      FileSystemFolder:
        "/TechtonFiles/" + req.body.EnterpriseName + RandomNumber,
    },
    collectionEnterprise
  );
  console.log(Enterprise.ops);
  const EnterpriseAdded = Enterprise.ops[0];
  const admin = await MongoWraper.UpdateMongo(
    { Email: RequestUser.Email },
    {
      AppProfile: "admin",
      EnterpriseId: EnterpriseAdded._id,
      EnterpriseName: EnterpriseAdded.EnterpriseName,
      LicenseId: req.body.LicenseId,
    },
    collectionUsers
  );
  res.json({
    status: "ok",
    message: "Enterpriese added, admin user:" + RequestUser.Email,
  });
};

exports.Register = Register;
