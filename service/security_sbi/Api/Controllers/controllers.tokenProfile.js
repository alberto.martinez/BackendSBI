const utils = require("../../helpers/helpers");
const router = require("express").Router();
var MongoWraper = require("../../helpers/mongoFunctions");
const { ObjectID } = require("mongodb");
const { ApiError } = require("../ApiError");
const {
  RegisterUser,
  InvitationMail,
  GetRandom6,
  ReSendValidation,
} = require("../helpersApi/RegisterUser");
const collectionUsers = "Users";
const collectionInvitation = "Invitation";

// {"Id":""}
const TokenValidation = async (req, res) => {
  console.log(req.params);
  try {
    const User = await MongoWraper.FindIDOne(req.params.Id, "Users");
    console.log(User);
    if (!User[0].Active) {
      throw new ApiError(400, "User not active");
    }
    if (User[0].EnterpriseId != null) {
      console.log("entre al if de enterprise");
      const Enterprise = await MongoWraper.FindIDOne(
        User[0].EnterpriseId,
        "Enterprise"
      );

      res.json({
        status: "ok",
        message: "User found",
        data: {
          user: JSON.stringify(User[0]),
          enterprise: JSON.stringify(Enterprise[0]),
        },
      });
    } else {
      res.json({
        status: "ok",
        message: "User found",
        data: {
          user: JSON.stringify(User[0]),
        },
      });
    }
  } catch (err) {
    throw new ApiError(401, "User not found");
  }
};

exports.TokenValidation = TokenValidation;
