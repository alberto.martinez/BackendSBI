const express = require("express");
const router = express.Router();
const Controllers = require("../Controllers/controllers.user");
const CheckToken = require("../../config/checkToken");

const catchErrAsync = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch((err) => next(err));
  };
};

const catchErr = (fn) => {
  return (req, res, next) => {
    try {
      fn(req, res, next);
    } catch (err) {
      next(err);
      //console.log(err)
    }
  };
};

router.route("/login").post(catchErrAsync(Controllers.Login));
router.route("/activate").post(catchErrAsync(Controllers.ActivateUser));
router.route("/register").post(catchErrAsync(Controllers.Register));
router
  .route("/register/knesys")
  .post(catchErrAsync(Controllers.RegisterKnesys));
router.route("/resendcode").post(catchErrAsync(Controllers.ReSendCode));
router.route("/invite").post(catchErrAsync(Controllers.InviteUser));
router.route("/join").post(catchErrAsync(Controllers.RegisterInvitation));

// router.route("/profile").get(CheckToken, catchErr(Controllers.Profile));
router.route("/profile").get(catchErr(Controllers.Profile));
module.exports = {
  router: router,
};
