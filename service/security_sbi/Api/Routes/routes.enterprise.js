const express = require("express");
const router = express.Router();
const Controllers = require("../Controllers/controllers.enterprise");
// const CheckToken = require("../../config/checkToken");

const catchErrAsync = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch((err) => next(err));
  };
};

const catchErr = (fn) => {
  return (req, res, next) => {
    try {
      fn(req, res, next);
    } catch (err) {
      next(err);
      //console.log(err)
    }
  };
};

router.route("/register").post(catchErrAsync(Controllers.Register));

module.exports = {
  router: router,
};
