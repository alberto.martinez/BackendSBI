const axios = require("axios");
const Helpers = require("../helpersApi/TemplateMail");
const utils = require("../../helpers/helpers");
var MongoWraper = require("../../helpers/mongoFunctions");
const { ApiError } = require("../ApiError");
const { ObjectID } = require("mongodb");
var QRCode = require("qrcode");
const collectionUsers = "Users";
function GetRandom6() {
  const min = 100000;
  const max = 999999;
  return Math.round(Math.random() * (max - min) + min);
}

const RegisterUser = async (user) => {
  const saltHash = utils.genPassword(user.Password);
  const salt = saltHash.salt;
  const hash = saltHash.hash;
  const ValidationCode = GetRandom6();
  const UserExist = await MongoWraper.FindOne(
    { Email: user.Email },
    collectionUsers
  );
  console.log(UserExist);
  if (UserExist != null) {
    if (UserExist.Active) {
      throw new ApiError(400, "User is already active");
    }
    const admin = await MongoWraper.UpdateMongo(
      { $or: [{ Username: user.UserName }, { Email: user.Email }] },
      {
        Email: user.Email,
        Name: user.Name,
        AppProfile: user.AppProfile,
        EnterpriseId: null,
        LastValidationCode: ValidationCode,
        Active: false,
        hash: hash,
        salt: salt,
      },
      collectionUsers
    );
  }
  //409 conflict
  else {
    const admin = await MongoWraper.SavetoMongo(
      {
        Email: user.Email,
        Name: user.Name,
        AppProfile: user.AppProfile,
        EnterpriseId: null,
        LastValidationCode: ValidationCode,
        Active: false,
        hash: hash,
        salt: salt,
      },
      collectionUsers
    );
  }
  ConfirmationMail(user.Email, ValidationCode);
};
const ReSendValidation = async (email) => {
  const ValidationCode = GetRandom6();
  const UserExist = await MongoWraper.FindOne(
    { Email: email },
    collectionUsers
  );
  console.log(UserExist);
  if (UserExist != null) {
    if (UserExist.Active) {
      throw new ApiError(400, "User is already active");
    }
    const admin = await MongoWraper.UpdateMongo(
      { Email: email },
      {
        LastValidationCode: ValidationCode,
      },
      collectionUsers
    );
  }
  //409 conflict
  else {
    throw new ApiError(400, "User does not exist");
  }
  ConfirmationMail(email, ValidationCode);
};
const ConfirmationMail = async (userMail, validationCode) => {
  try {
    const Base64QR = await QRCode.toDataURL(validationCode.toString());
    // console.log(userMail);
    const msgSNMP = {
      api_key: "api-B90B95F2080E11EB91D0F23C91C88F4E",
      sender: "soporte@knesysplus.com",
      to: [userMail],
      subject: "Nuevo usuario requiere aprovación",
      html_body: Helpers.GenerateHTML(
        "Verifique su dirección de correo electrónico",
        "Para comenzar a usar su cuenta de Knesys, debe confirmar ingresar el codigo de confirmación en la app móvil knesys+.",
        validationCode,
        Base64QR,
        "Si no se registró para esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.",
        "Obtenga la última aplicación de Knesys para su teléfono"
      ),
    };
    console.log("Sending Email");
    const Mail = await axios.post(
      "https://api.smtp2go.com/v3/email/send",
      msgSNMP
    );

    console.log(Mail.data.data);
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

const InvitationMail = async (
  userMail,
  enterprise,
  validationCode,
  appProfile
) => {
  try {
    const Base64QR = await QRCode.toDataURL(validationCode.toString());
    // console.log(Base64QR);
    // console.log(userMail);
    const msgSNMP = {
      api_key: "api-B90B95F2080E11EB91D0F23C91C88F4E",
      sender: "soporte@knesysplus.com",
      to: [userMail],
      subject: "Invitación a " + enterprise,
      html_body: Helpers.GenerateHTML(
        "Forme parte de" + enterprise,
        "Para comenzar a operar en " +
          enterprise +
          " con el perfil de " +
          appProfile +
          " ingresar el codigo de acceso en la app móvil knesys+.",
        validationCode,
        Base64QR,
        "Si no pertenece a esta empresa, puede ignorar este correo electrónico",
        "Obtenga la última aplicación de Knesys para su teléfono"
      ),
    };
    console.log("Sending Email");
    const Mail = await axios.post(
      "https://api.smtp2go.com/v3/email/send",
      msgSNMP
    );

    console.log(Mail.data.data);
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};
exports.InvitationMail = InvitationMail;
exports.RegisterUser = RegisterUser;
exports.GetRandom6 = GetRandom6;
exports.ReSendValidation = ReSendValidation;
