const GenerateHTML = (
  header,
  instructions,
  validationCode,
  Qr,
  footer,
  movileFooter
) => {
  const QrTest =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATwAAACgCAMAAACmCCC4AAAAxlBMVEX///9u8AEBsf/BwcH5+fm+vr709PTT09MArf/r6+va2trCwsL8/Pyf3v+u5P8Jtf/Kysrh4eHx+//g4ODo6Ohcy/8evf/Ozs5l7wDq/dlw8AD9//rz/e7v/eLy/uf5/vPV8f/i/M7U+rvg/MiJ8znc/MFBwP+2949+8hTs+v+t3f980P+++KGq9nvN+bSd9Gi/6f+x94aP80uJ8y7A+KPj/NTd9f+q9nOg9ltx0f/A+Zed9GzJ+aqY9F+S81Ga3P+Dzf+S9D99t4OOAAAJSklEQVR4nO2cDVPiSBCGifkyHytB4iYiIrqoeIiKwrLnip7//09dz4RAkulJAnp1LtNPbdVuJUyn503PTPck2UaDIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIP5Xbm4OP2ag2z37HE/+OPp/jUajb/2t25+c347Hk7ve53n053BwtQ/sXR34WzU/Ob9+j4HO/eyTHfsDOGjuJTQftmk+m7xrCfFj97N9++ocHi2129u/2nziOx4POlpK/HzyHzj4lXn4vrdSr79h2958uJYOeFIt9L411+IdbNSyO461PDGJV4uT04lW1I7Eq8XJ8fVQkE7ThiReNcdTTDotHquWKW8u3mz6hEmnxU/KJXqbije7HaDSQZp3oVqmsqF4s+tHVDkt7ozlcXdzcPDzM33+Mmwi3unksYNLF89n8sXi5qjZvOp/qtdfhPrinWWribx2v07LloqHfbD99wf3bb4ktcUbS5TT4vfT0snOfwPx9r6rK97ZJMaXCU17Pa64guLidZ87EumGix+VV1BavN6FbIV9+n1R4woKi3d68SqRbvj7rtYVlBXv9G4h1v+JdPO7msWYouL1Xu7xyS7uzC9qbwIoKV53upCkxNr8coP9ExXFm74OJZPdr/ONtp7UE28qrSbezzes/1UT7+RdktfFj9V5XRHFxDuT7Dp1fl3WNeuvaBym4mWO7QiYeNfokO08vtQdsP1vWfjDzeZb9tDDbsQhJt4CCbzO63Pt9yn63/ezLK3nDo12Qj1EvB5SUyymp/VtjvYq2Y39PUS8Y6SWva9Txab8s6+ueCcLZJkdjuvLd9CsUm9/Z4ctvmDET7/rrrWHD38fZWHGm7kjb7vxTANNVZ7QTEUbLs5rWj3McJOkKj+zx3YkWUGT5J5833jzlwL8b0olycBcVp1p402fzipWYTCOZc/K4s5zdyP9FBSPvfM5kETf8Ln8eVkeJcVrNM4nkg2CeHA7qy2fouKxl7UlewTa4LnuyquqeFw+/J0oTXu8rvdWlLriQdI3Q19k5PJNqh54M1QWD+Q7nkgeencGk+r9ArXFY28gj2Vp82BatXKoLh7QfcfFg8SlQj4SD7iUPk4bvpQ9/+blmeKvmAEX95Idg3hw15OH38+rbb/N+ups9Frt2d1vSfRpjy/yXfr+aLST2m36QvfZxViWuNzXyVt2i40/JehezCUvsCyU++J2i49Yupe4fPGtat8SbPX5VPfHPRZ77/QFUC3OfiAbLvThXm3uhJVDOfE+8rHy9Cm/2dxRTbyb1Wfy21RQt9lPguK5anPeB/+DhrPpIk2b4yf1Er3DN/58f7/5tl312XtZ8MQlfrxULVNhPIyOrq5G27/1dXo3Hwxer9WLO87hz37/5iMGeuezYxXDjiAIgiAIgiAIgiAIgiAI4othoB9s+UZyEm2R/gRl9SvZiTIEm+VfkyEurE6VtEIPpkcNq4BjSS1ZtiueNEI7csBgy26JV3Js2+cNcULuiuMKJ9oWb9UWVbRclx80WqK5VonoDnJ5N2TOgeeOTD4HswkNrOXfURDAnwwR4nLSyDX1UDQf6KYNnfJ0zy429F3dhOv4bVPHMCOmghuIZ7zAavi26dnFm2VFuhnyvz2klStVz7AxF7jHVmAKjq97HIjh4gSsww2jjbig6238Rvh24ni2LzYzwI5apse7XHBZNyEqG6FuorTZ7WMGCsARONUyPY8FdYaW5yU+RGgrPf/rrKOR7gmXhwYRE08X7tG6x4FoMgxYM+gUuBBEOVgc4LYE8ZbS2cw+iAcd01uoeJn/kiNHg7tusmGTPcruacTvu5fXo82ihc0OIVzNtfLGLLgYMnOsxDNDp4DrMfeYeLLIKxHPAItCu1WHq8Sz+EhIRxYXz/NMO+u+3NbaExNxHboa8JNgn8Xg8voRiJl0BgLPFUw5Zrl4YkiAeva2kWeZSDM2T1WL51sudAykS3/KbIUwNvTs0K0hXsssRCs3Zuse/4cTwP1YzmQWGxNLfyOsU05F5GGz14fEc8WrwbxSNWx9qx1A2HnROhCZeAYfx966B/XEQ3xw0pvCZNQjtuhAR9crAognLF0wfSLJQOoeKl7rI5Gn68hi4oeS/q7Es9oRky63jCdR7Ld5F1OjpXNe8hMHwjc00FOJCYhw6IEB/dSDVYzCwcgpaSWwFC/3e4MddLcVD+Y83XYKeZ48V0rFC0Xp1lOAAyfNdI1MxYPkTCRpDz/RA7t4KtsZlhEEkBDpmZXXYseKBsMS+RLx/HyDAKwY26+2bJTlF1tb7kMqHsSK5xVHyGr+tNx1jCzFk+R5iQUnEE96UdY6v1mwtGWPtcQcy/RkCepKPKt4KS/cPlXxsTwvQOagnHiwynpeUFBvvfgYrWCZfq7FQ5IsM1p2FdQW07zcCswmPjN/Pd+xi3keXFSSoK7EM/Jtkjl7W/FYaZQvL1gky3xIxTNCvloEuXGbXblhbvf4IFuLZ4bFMnA9P/jFMyx28wlloEfF/hXrSgfqH2QGz4nXwFzYWjyxXrbYHFRVYUBwMfm8zAjPpT1sImNDNyOePPcXL+YnSXLmN0Iei7QyxAIo83t0tV0Z30Y8C6mI5ZfJ5XmtIJkz0j4VckY2m8IsVS1eaCLugQ+CeDmfXDMSPYd8WzpuK8TbosKwTFOc3+olyUwRNsTNYClfMeF2WLEGs0CFeOxqYrg4QYV4Jtap1sZJ8sq4XDwPEY+PCwdLUOtFHsPga70ZcfmEasXgCfNKvFCyn8eS5OJhFrDtrC1BvACS5GIrlkWXbQyUiBdZiHN+kiYUS2h/6R10GFLN4mwNayl+I8RdFWcpn4WVemxkr8TTI2w/zWE1FSRxBYJi8i6IZyOtwBNhVakpnmAr3VOEFN6M2q0srEBgJZjBR1YBWChlqy0yxBy2G8f380TXfah10zwP3foyA5aoYzlgofgSxHNwc/JSkIWLXDzcu/Zqase843tpAvItxdAT3WNlrtfi1YqoOSQdPIRCJM1jeZbLuyWc9Ipbu2C84FQrEpLDwJZrx4I/knTLsFHnko1fpl7+7sJoTVOOSIi8ks1s30EyBJCPd9VykHZGsqlvFHfSlhiSk0KMiMYtsVHpkw9D/nhBNJXgL3sXtjO0wrUhIXUtd0FNam8/EARBEARBEARBEARBEARBEARBEARBEARBEARBqMW/bXbSeXOJrZIAAAAASUVORK5CYII=";
  const MailTemplate = `
<head>
  <meta charset="UTF-8">
  <title> ${header}</title>
</head>

<body>

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Knesys</title>
  </head>
  
  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"
  style="margin: 0pt auto; padding: 0px; background:#F4F7FA;">
    <table id="main" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0"
    bgcolor="#F4F7FA">
      <tbody>
        <tr>
          <td valign="top">
            <table class="innermain" cellpadding="0" width="580" cellspacing="0" border="0"
            bgcolor="#F4F7FA" align="center" style="margin:0 auto; table-layout: fixed;">
              <tbody>
                <!-- START of MAIL Content -->
                <tr>
                  <td colspan="4">
                    <!-- Logo start here -->
                    <table class="logo" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tbody>
                        <tr>
                          <td colspan="2" height="30"></td>
                        </tr>
                        <tr>
                          <td valign="top" align="center">
                            <a href="https://www.coinbase.com" style="display:inline-block; cursor:pointer; text-align:center;">
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATwAAACgCAMAAACmCCC4AAAAxlBMVEX///9u8AEBsf/BwcH5+fm+vr709PTT09MArf/r6+va2trCwsL8/Pyf3v+u5P8Jtf/Kysrh4eHx+//g4ODo6Ohcy/8evf/Ozs5l7wDq/dlw8AD9//rz/e7v/eLy/uf5/vPV8f/i/M7U+rvg/MiJ8znc/MFBwP+2949+8hTs+v+t3f980P+++KGq9nvN+bSd9Gi/6f+x94aP80uJ8y7A+KPj/NTd9f+q9nOg9ltx0f/A+Zed9GzJ+aqY9F+S81Ga3P+Dzf+S9D99t4OOAAAJSklEQVR4nO2cDVPiSBCGifkyHytB4iYiIrqoeIiKwrLnip7//09dz4RAkulJAnp1LtNPbdVuJUyn503PTPck2UaDIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIP5Xbm4OP2ag2z37HE/+OPp/jUajb/2t25+c347Hk7ve53n053BwtQ/sXR34WzU/Ob9+j4HO/eyTHfsDOGjuJTQftmk+m7xrCfFj97N9++ocHi2129u/2nziOx4POlpK/HzyHzj4lXn4vrdSr79h2958uJYOeFIt9L411+IdbNSyO461PDGJV4uT04lW1I7Eq8XJ8fVQkE7ThiReNcdTTDotHquWKW8u3mz6hEmnxU/KJXqbije7HaDSQZp3oVqmsqF4s+tHVDkt7ozlcXdzcPDzM33+Mmwi3unksYNLF89n8sXi5qjZvOp/qtdfhPrinWWribx2v07LloqHfbD99wf3bb4ktcUbS5TT4vfT0snOfwPx9r6rK97ZJMaXCU17Pa64guLidZ87EumGix+VV1BavN6FbIV9+n1R4woKi3d68SqRbvj7rtYVlBXv9G4h1v+JdPO7msWYouL1Xu7xyS7uzC9qbwIoKV53upCkxNr8coP9ExXFm74OJZPdr/ONtp7UE28qrSbezzes/1UT7+RdktfFj9V5XRHFxDuT7Dp1fl3WNeuvaBym4mWO7QiYeNfokO08vtQdsP1vWfjDzeZb9tDDbsQhJt4CCbzO63Pt9yn63/ezLK3nDo12Qj1EvB5SUyymp/VtjvYq2Y39PUS8Y6SWva9Txab8s6+ueCcLZJkdjuvLd9CsUm9/Z4ctvmDET7/rrrWHD38fZWHGm7kjb7vxTANNVZ7QTEUbLs5rWj3McJOkKj+zx3YkWUGT5J5833jzlwL8b0olycBcVp1p402fzipWYTCOZc/K4s5zdyP9FBSPvfM5kETf8Ln8eVkeJcVrNM4nkg2CeHA7qy2fouKxl7UlewTa4LnuyquqeFw+/J0oTXu8rvdWlLriQdI3Q19k5PJNqh54M1QWD+Q7nkgeencGk+r9ArXFY28gj2Vp82BatXKoLh7QfcfFg8SlQj4SD7iUPk4bvpQ9/+blmeKvmAEX95Idg3hw15OH38+rbb/N+ups9Frt2d1vSfRpjy/yXfr+aLST2m36QvfZxViWuNzXyVt2i40/JehezCUvsCyU++J2i49Yupe4fPGtat8SbPX5VPfHPRZ77/QFUC3OfiAbLvThXm3uhJVDOfE+8rHy9Cm/2dxRTbyb1Wfy21RQt9lPguK5anPeB/+DhrPpIk2b4yf1Er3DN/58f7/5tl312XtZ8MQlfrxULVNhPIyOrq5G27/1dXo3Hwxer9WLO87hz37/5iMGeuezYxXDjiAIgiAIgiAIgiAIgiAI4othoB9s+UZyEm2R/gRl9SvZiTIEm+VfkyEurE6VtEIPpkcNq4BjSS1ZtiueNEI7csBgy26JV3Js2+cNcULuiuMKJ9oWb9UWVbRclx80WqK5VonoDnJ5N2TOgeeOTD4HswkNrOXfURDAnwwR4nLSyDX1UDQf6KYNnfJ0zy429F3dhOv4bVPHMCOmghuIZ7zAavi26dnFm2VFuhnyvz2klStVz7AxF7jHVmAKjq97HIjh4gSsww2jjbig6238Rvh24ni2LzYzwI5apse7XHBZNyEqG6FuorTZ7WMGCsARONUyPY8FdYaW5yU+RGgrPf/rrKOR7gmXhwYRE08X7tG6x4FoMgxYM+gUuBBEOVgc4LYE8ZbS2cw+iAcd01uoeJn/kiNHg7tusmGTPcruacTvu5fXo82ihc0OIVzNtfLGLLgYMnOsxDNDp4DrMfeYeLLIKxHPAItCu1WHq8Sz+EhIRxYXz/NMO+u+3NbaExNxHboa8JNgn8Xg8voRiJl0BgLPFUw5Zrl4YkiAeva2kWeZSDM2T1WL51sudAykS3/KbIUwNvTs0K0hXsssRCs3Zuse/4cTwP1YzmQWGxNLfyOsU05F5GGz14fEc8WrwbxSNWx9qx1A2HnROhCZeAYfx966B/XEQ3xw0pvCZNQjtuhAR9crAognLF0wfSLJQOoeKl7rI5Gn68hi4oeS/q7Es9oRky63jCdR7Ld5F1OjpXNe8hMHwjc00FOJCYhw6IEB/dSDVYzCwcgpaSWwFC/3e4MddLcVD+Y83XYKeZ48V0rFC0Xp1lOAAyfNdI1MxYPkTCRpDz/RA7t4KtsZlhEEkBDpmZXXYseKBsMS+RLx/HyDAKwY26+2bJTlF1tb7kMqHsSK5xVHyGr+tNx1jCzFk+R5iQUnEE96UdY6v1mwtGWPtcQcy/RkCepKPKt4KS/cPlXxsTwvQOagnHiwynpeUFBvvfgYrWCZfq7FQ5IsM1p2FdQW07zcCswmPjN/Pd+xi3keXFSSoK7EM/Jtkjl7W/FYaZQvL1gky3xIxTNCvloEuXGbXblhbvf4IFuLZ4bFMnA9P/jFMyx28wlloEfF/hXrSgfqH2QGz4nXwFzYWjyxXrbYHFRVYUBwMfm8zAjPpT1sImNDNyOePPcXL+YnSXLmN0Iei7QyxAIo83t0tV0Z30Y8C6mI5ZfJ5XmtIJkz0j4VckY2m8IsVS1eaCLugQ+CeDmfXDMSPYd8WzpuK8TbosKwTFOc3+olyUwRNsTNYClfMeF2WLEGs0CFeOxqYrg4QYV4Jtap1sZJ8sq4XDwPEY+PCwdLUOtFHsPga70ZcfmEasXgCfNKvFCyn8eS5OJhFrDtrC1BvACS5GIrlkWXbQyUiBdZiHN+kiYUS2h/6R10GFLN4mwNayl+I8RdFWcpn4WVemxkr8TTI2w/zWE1FSRxBYJi8i6IZyOtwBNhVakpnmAr3VOEFN6M2q0srEBgJZjBR1YBWChlqy0yxBy2G8f380TXfah10zwP3foyA5aoYzlgofgSxHNwc/JSkIWLXDzcu/Zqase843tpAvItxdAT3WNlrtfi1YqoOSQdPIRCJM1jeZbLuyWc9Ipbu2C84FQrEpLDwJZrx4I/knTLsFHnko1fpl7+7sJoTVOOSIi8ks1s30EyBJCPd9VykHZGsqlvFHfSlhiSk0KMiMYtsVHpkw9D/nhBNJXgL3sXtjO0wrUhIXUtd0FNam8/EARBEARBEARBEARBEARBEARBEARBEARBEARBqMW/bXbSeXOJrZIAAAAASUVORK5CYII="
                              height="110" width="144" border="0" alt="Coinbase">
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" height="30"></td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- Logo end here -->
                    <!-- Main CONTENT -->
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff"
                    style="border-radius: 4px; box-shadow: 0 2px 8px rgba(0,0,0,0.05);">
                      <tbody>
                        <tr>
                          <td height="40"></td>
                        </tr>
                        <tr style="font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif; color:#4E5C6E; font-size:14px; line-height:20px; margin-top:20px;">
                          <td class="content" colspan="2" valign="top" align="center" style="padding-left:90px; padding-right:90px;">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
                              <tbody>
                                <tr>
                                  <td align="center" valign="bottom" colspan="2" cellpadding="3">
                                    <img alt="Coinbase" width="80" src="https://www.coinbase.com/assets/app/icon_email-e8c6b940e8f3ec61dcd56b60c27daed1a6f8b169d73d9e79b8999ff54092a111.png"
                                    />
                                  </td>
                                </tr>
                                <tr>
                                  <td height="30" &nbsp;=""></td>
                                </tr>
                                <tr>
                                  <td align="center"> <span style="color:#48545d;font-size:22px;line-height: 24px;">
          Verify your email address
        </span>

                                  </td>
                                </tr>
                                <tr>
                                  <td height="24" &nbsp;=""></td>
                                </tr>
                                <tr>
                                  <td height="1" bgcolor="#DAE1E9"></td>
                                </tr>
                                <tr>
                                  <td height="24" &nbsp;=""></td>
                                </tr>
                                <tr>
                                  <td align="center"> <span style="color:#48545d;font-size:14px;line-height:24px;">
          ${instructions}
        </span>

                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" &nbsp;=""></td>
                                </tr>
                                <tr>
                                  <td valign="top" width="48%" align="center"> <span>
          <div style="display:block; padding:15px 25px; background-color:#0087D1; color:#ffffff; border-radius:3px; text-decoration:none;">${validationCode}</div>
         <img src=${Qr} height="110" width="110" border="0" alt="Qr">
                            
          </span>

                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" &nbsp;=""></td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <img src="https://s3.amazonaws.com/app-public/Coinbase-notification/hr.png" width="54"
                                    height="2" border="0">
                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" &nbsp;=""></td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <p style="color:#a2a2a2; font-size:12px; line-height:17px; font-style:italic;">${footer}</p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="60"></td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- Main CONTENT end here -->
                    <!-- PROMO column start here -->
                    <!-- Show mobile promo 75% of the time -->
                    <table id="promo" width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">
                      <tbody>
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center"> <span style="font-size:14px; font-weight:500; margin-bottom:10px; color:#7E8A98; font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif;">${movileFooter}</span>

                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                        <tr>
                          <td valign="top" width="50%" align="right">
                            <a href="https://itunes.apple.com/us/app/coinbase-buy-bitcoin-more/id886427730?mt=8"
                            style="display:inline-block;margin-right:10px;">
                              <img src="https://s3.amazonaws.com/app-public/Coinbase-email/iOS_app.png" height="40"
                              border="0" alt="Coinbase iOS mobile bitcoin wallet">
                            </a>
                          </td>
                          <td valign="top">
                            <a href="https://play.google.com/store/apps/details?id=com.knesys.datacenter_app"
                            style="display:inline-block;margin-left:5px;">
                              <img src="https://s3.amazonaws.com/app-public/Coinbase-email/Android_app.png"
                              height="40" border="0" alt="Coinbase Android mobile bitcoin wallet">
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- PROMO column end here -->
                    <!-- FOOTER start here -->
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tbody>
                        <tr>
                          <td height="10">&nbsp;</td>
                        </tr>
                        <tr>
                          <td valign="top" align="center"> <span style="font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif; color:#9EB0C9; font-size:10px;">&copy;
                            <a href="https://www.coinbase.com/" target="_blank" style="color:#9EB0C9 !important; text-decoration:none;">Coinbase</a> 2017
                          </span>

                          </td>
                        </tr>
                        <tr>
                          <td height="50">&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- FOOTER end here -->
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>

</html>
  
  

</body>`;

  return MailTemplate;
};

exports.GenerateHTML = GenerateHTML;
