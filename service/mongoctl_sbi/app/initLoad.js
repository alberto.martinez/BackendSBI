// initLoad.js
require("dotenv").config();
const { MongoClient, ObjectID } = require("mongodb");
const { mongoUri, mongoDb } = require("../config/vars");

async function insertDefaultUser() {
  try {
    const db = await MongoClient.connect(mongoUri, {
      useUnifiedTopology: true,
    });
    const dbo = db.db(mongoDb);
    return await dbo.collection("usersList").insertOne({
      _id: ObjectID("5f03751d6c36d900321bf6b9"),
      user: "suni.lozano@grupoabsa.com",
      role: "admin",
    });
  } catch (e) {
    console.log(`Error Insert Default User: ${e}`);
  }
}

async function initLoad() {
  try {
    // const serverData = await insertDefaultUser();
    // if (serverData == null) {
    //   console.log("Setting up default user 1...");
    //   await insertDefaultUser();
    // } else console.log(`Default user 1...`);
  } catch (e) {
    console.log(`Error: ${e}`);
  }
}

initLoad();

module.exports.initLoad = initLoad;
