require("dotenv").config();

module.exports = {
  env: process.env.ENV,
  mongoUri: process.env.MONGO_URI,
  mongoDb: process.env.MONGO_DB,
  port: process.env.MONGOCTL_PORT,
};
