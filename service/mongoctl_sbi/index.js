// index.js
require('dotenv').config();
const { getDateTime } = require('./app/getDateTime'); // Import getDateTime file

module.exports = require("./app/initLoad");

// Setup server port
var port = process.env.MONGOCTL_PORT;

console.log('Mongoctl Container runs on port ' + port +' at: ' + getDateTime());
