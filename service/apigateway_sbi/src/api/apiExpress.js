// apiExpress.js
const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

exports.service1 = function(){
    const app1 = express();
    const port1 = process.env.PORT || 3000;

    app1.use(bodyParser.urlencoded({ extended: true}));
    app1.use(bodyParser.json());

    app1.get('/', (req, res) => res.send(`Hello World port ${port1}`));

    app1.listen(port1, () => {
        console.log(`APIREST-tokenGenerator runs on port ${port1}...`);
    });

    app1.post('/login', (req, res) => {
        if(req.body.email==="knesys@knesys.com" && req.body.password==="knesys123" && req.body.name==="knesys"){
            const payload = {
                check:  true
            };
          
            const token = jwt.sign(payload, 'miclaveultrasecreta123*', {
                expiresIn: '1h'
            });
            
            res.json({
                mensaje: 'Autenticación correcta',
                token: {
                    tokenType: 'Bearer',
                    accessToken: token
                }
            });
        } else {
          res.json({ mensaje: "Usuario o contraseña incorrectos"}); // agregar status http
        }
    });  
}

exports.service2 = function(){
    const app2 = express();
    const port2 = process.env.PORT || 3002;

    app2.use(bodyParser.urlencoded({ extended: false}));
    app2.use(bodyParser.json());

    app2.get('/get', (req, res) => {
        res.send('Hello World port 3002!');
    });

    app2.listen(port2, () => {
        console.log(`API REST runs on port ${port2}...`);
    });
}