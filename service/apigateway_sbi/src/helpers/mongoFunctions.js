// mongofunctions.js
const { mongo, mongoDb } = require("../../config/vars");
const { MongoClient, Timestamp } = require("mongodb");
var ObjectId = require("mongodb").ObjectID;

async function FindIDOne(Id, collection) {
  var o_id = new ObjectId(Id);
  const query = { _id: o_id };
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo
      .collection(collection)
      .find(query)
      .limit(1)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}
function SavetoMongoCallback(objectToSave, collection) {
  MongoClient.connect(mongo.uri, { useUnifiedTopology: true }, (err, db) => {
    if (err) console.log(err);
    const dbo = db.db(mongoDb);
    dbo.collection(collection).insertOne(objectToSave, (err2) => {
      if (err2) console.log(err2);
      console.log("inserted");
      //console.log(`- 1 Device inserted: ${finalObject.macdeviceid} -- ${finalObject.sensor.counter.value}`);
      db.close();
    });
  });
}

async function SavetoMongo(objectToSave, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo.collection(collection).insertOne(objectToSave);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}

async function AggregationMongo(arrAggregation, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo
      .collection(collection)
      .aggregate(arrAggregation, { allowDiskUse: true })
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log("errooooooor");
    console.log(error);
    return [];
  }
}

async function DeleteMongo(query, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo.collection(collection).deleteOne(query);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}

async function UpdateMongo(query, newProperties, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    var newvalues = { $set: newProperties };
    let result = await dbo.collection(collection).updateOne(query, newvalues);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}
async function UpsertMongo(query, newProperties, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    var newvalues = { $set: newProperties };
    let result = await dbo
      .collection(collection)
      .updateOne(query, newvalues, { upsert: true });
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}
async function UpsertMongoCounter(query, newProperties, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    var newvalues = { $inc: newProperties };
    let result = await dbo
      .collection(collection)
      .updateOne(query, newvalues, { upsert: true });
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
}

function DeleteMongoCallback(idObjectToDelete, collection) {
  MongoClient.connect(mongo.uri, { useUnifiedTopology: true }, (err, db) => {
    if (err) console.log(err);
    const dbo = db.db(mongoDb);
    var myquery = { _id: idObjectToDelete };
    dbo.collection(collection).deleteOne(myquery, (err2) => {
      if (err2) console.log(err2);
      console.log("deleted");
      //console.log(`- 1 Device inserted: ${finalObject.macdeviceid} -- ${finalObject.sensor.counter.value}`);
      db.close();
    });
  });
}

async function GetLastMongo(limit, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo
      .collection(collection)
      .find()
      .sort({ _id: 1 })
      .limit(limit)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

async function FindOne(query, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo.collection(collection).findOne(query);
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return {};
  }
}

async function FindManyLimit(query, limit, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo
      .collection(collection)
      .find(query)
      .limit(limit)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

async function GetAll(collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo
      .collection(collection)
      .find({})
      .sort({ _id: 1 })
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}
async function FindLimitLast(query, limit, collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo
      .collection(collection)
      .find(query)
      .sort({ _id: -1 })
      .limit(limit)
      .toArray();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

async function DropCollection(collection) {
  try {
    let db = await MongoClient.connect(mongo.uri, { useUnifiedTopology: true });
    const dbo = db.db(mongoDb);
    let result = await dbo.collection(collection).drop();
    await db.close();
    //console.log(util.inspect(result, false, null, true /* enable colors */))
    return result;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}
exports.UpsertMongoCounter = UpsertMongoCounter;
exports.UpsertMongo = UpsertMongo;
exports.AggregationMongo = AggregationMongo;
exports.UpdateMongo = UpdateMongo;
exports.FindOne = FindOne;
exports.FindIDOne = FindIDOne;
exports.GetAll = GetAll;
exports.SavetoMongoCallback = SavetoMongoCallback;
exports.SavetoMongo = SavetoMongo;
exports.DropCollection = DropCollection;
exports.DeleteMongoCallback = DeleteMongoCallback;
exports.DeleteMongo = DeleteMongo;
exports.GetLastMongo = GetLastMongo;
exports.FindManyLimit = FindManyLimit;
exports.FindLimitLast = FindLimitLast;
