// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");
const axios = require("axios");
const URILicense = "http://license_tech:1010/validate/module/";
module.exports = (module) => async (req, res, next) => {
  // const token = req.headers['access-token'];

  try {
    const ModuleValidation = await axios.post(
      URILicense,
      { Module: module },
      {
        headers: {
          user: req.headers.user,
        },
      }
    );
    console.log(ModuleValidation.data);
    const Active = ModuleValidation.data.data.Active;
    console.log(Active);
    if (Active) {
      next();
    } else {
      return res.send(
        {
          status: "error",
          errors: "License module not active",
        },
        402
      );
    }
  } catch (err) {
    return res.send(err.response.data);
  }
};
