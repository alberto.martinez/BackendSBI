// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");
const axios = require("axios");
const URILicense = "http://license_tech:1010/validate/";
module.exports = async (req, res, next) => {
  // const token = req.headers['access-token'];
  console.log("entre a validacion de licensia");
  try {
    const LicenseValidation = await axios.post(
      URILicense,
      {},
      {
        headers: {
          user: req.headers.user,
        },
      }
    );
    console.log(LicenseValidation.data);
    const Active = LicenseValidation.data.data.Active;
    console.log(Active);
    if (Active) {
      next();
    } else {
      return res.send(
        {
          status: "error",
          errors: "License not active",
        },
        402
      );
    }
  } catch (err) {
    return res.send(err.response.data);
  }
};
