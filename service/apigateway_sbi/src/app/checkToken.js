// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");
const axios = require("axios");
const URISecurity = "http://security_tech:7030/token/info/";
module.exports = async (req, res, next) => {
  // const token = req.headers['access-token'];
  var token = req.headers.authorization;

  if (token) {
    token = token.replace("Bearer ", "");
    console.log(token);
    jwt.verify(token, "miclaveultrasecreta123", async (err, decoded) => {
      console.log(decoded);
      if (err) {
        console.log("err");
        console.log(res);
        return res.send(
          {
            status: "error",
            errors: "Invalid Token",
          },
          401
        );
      }
      try {
        console.log(decoded.sub);
        const UserProfile = await axios.get(
          URISecurity + decoded.sub.toString()
        );
        console.log(UserProfile.data);
        console.log(UserProfile.data.data);
        req.headers = { ...req.headers, ...UserProfile.data.data };
        next();
      } catch (err) {
        console.log("Entre a condicional");
        return res.send(
          {
            status: "error",
            errors: "User not found",
          },
          401
        );
      }
    });
  } else {
    console.log("console.log err");
    return res.status(400).send(
      {
        status: "error",
        errors: "Token no not found",
      },
      401
    );
  }
};
