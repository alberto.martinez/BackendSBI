// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");

module.exports = (req, res, next) => {
  // const token = req.headers['access-token'];
  var token = req.headers.authorization;

  if (token) {
    token = token.replace("Bearer ", "");
    console.log(token);
    jwt.verify(token, "miclaveultrasecreta123", async (err, decoded) => {
      console.log(decoded);
      if (err) {
        console.log("err");
        return res.send({
          status: "error",
          errors: "Token inválida",
        });
      }
      try {
        const User = await MongoWraper.FindIDOne(decoded.sub, "Users");
        console.log(User);
        console.log(User[0].Active);
        if (!User[0].Active) {
          return res.send({
            status: "error",
            errors: "User not active",
          });
        }
        if (User[0].EnterpriseId != null) {
          console.log("entre al if de enterprise");
          const Enterprise = await MongoWraper.FindIDOne(
            User[0].EnterpriseId,
            "Enterprise"
          );
          req.headers = {
            ...req.headers,
            user: JSON.stringify(User[0]),
            enterprise: JSON.stringify(Enterprise[0]),
          };
        } else {
          req.headers = { ...req.headers, user: JSON.stringify(User[0]) };
        }
        next();
      } catch (err) {
        return res.send(
          {
            status: "error",
            errors: "User not found",
          },
          401
        );
      }
    });
  } else {
    console.log("console.log err");
    return res.json({
      status: "error",
      errors: "Token no proveída",
    });
  }
};
