// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");
const axios = require("axios");
const URISecurity = "http://security_tech:7030/token/info/";
module.exports = async (req, res, next) => {
  const RequestUser = JSON.parse(req.headers.user);
  if (RequestUser.EnterpriseId) {
    next();
  } else {
    res.status(500).send({
      status: "error",
      errors: "User needs enterprise to use this feature",
    });
  }
};
