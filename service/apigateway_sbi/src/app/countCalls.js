// checkToken.js
const express = require("express");
const jwt = require("jsonwebtoken");
var MongoWraper = require("../helpers/mongoFunctions");
const axios = require("axios");
const URILicense = "http://license_tech:1010/validate/";
var MongoWraper = require("../helpers/mongoFunctions");
const collectionCount = "UsageCount";
module.exports = (module) => async (req, res, next) => {
  await MongoWraper.UpsertMongoCounter(
    { Module: module },
    // { $inc: { Count: 1 } },
    { Count: 1 },
    collectionCount
  );
  next();
};
