// index.js
const gateway = require("fast-gateway"); // Import fast-gateway library
const service = require("../api/apiExpress");
const checkToken = require("./checkToken");
const countCalls = require("./countCalls");
const validateLicenseModule = require("./validateLicenseModule");
const validateLicense = require("./validateLicense");
const verify = require("./verifyReq");
const log = require("./log");
const version = "v1";
const cors = require("cors");
const port = 8030;

// API Gateway
const server = gateway({
  routes: [
    // {
    //   prefix: "/api/v1/push/",
    //   target: "http://localhost:7030/login",
    //   middlewares: [],
    // },

    {
      prefix: "/api/" + version + "/user/invite/",
      target: "http://security_tech:7030/user/invite",
      middlewares: [cors(), checkToken],
    },
    {
      prefix: "/api/" + version + "/user/join/",
      target: "http://security_tech:7030/user/join",
      middlewares: [cors(), checkToken],
    },
    {
      prefix: "/api/" + version + "/user/profile/",
      target: "http://security_tech:7030/user/profile",
      middlewares: [cors(), checkToken],
    },
    {
      prefix: "/api/" + version + "/enterprise/register/",
      target: "http://security_tech:7030/enterprise/register",
      middlewares: [cors(), checkToken],
    },
    // {
    //   prefix: "/api/" + version + "/notification/device/",
    //   target: "http://notification_tech:6030/notification/device",
    //   middlewares: [cors(), checkToken],
    // },
    {
      prefix: "/api/" + version + "/notification",
      target: "http://notification_tech:6030/notification",
      middlewares: [
        cors(),
        checkToken,
        log,
        validateLicense,
        validateLicenseModule("Notification"),
        countCalls("Notification"),
      ],
    },

    {
      prefix: "/api/" + version + "/license",
      target: "http://license_tech:1010/",
      middlewares: [cors(), checkToken],
    },

    {
      prefix: "/api/" + version,
      target: "http://security_tech:7030/",
      middlewares: [cors()],
    },
  ],
});
server.start(port).then(() => {
  console.log("APIGATEWAY runs on port " + port + "...");
});

// service.service1();
